package cz.java5.p7;

public class Magazine {
    private int capacity = 15;
    private int bulletCount = 0;

    public void addBullet() {
        if (bulletCount < capacity) {
            bulletCount++;
        }

    }

    public boolean isLoaded() {
        return bulletCount > 0;
    }

    public void shoot() {
        if (bulletCount == 0) {
            throw new RuntimeException("empty magazine");
        }
        bulletCount--;
    }
}
