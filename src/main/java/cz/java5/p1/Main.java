package cz.java5.p1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String... args) {
        List<String> sourceList = Arrays.asList("A", "B", "C", "D","a");
        List<String> sortedList = orderList(sourceList);
        System.out.println(sortedList);


    }

    public static List<String> orderList(List<String> sourceList) {
        Collections.sort(sourceList, Collections.reverseOrder());
        // Collections.reverse(sourceList);
        return sourceList;
    }

}
