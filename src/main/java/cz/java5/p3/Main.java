package cz.java5.p3;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map1 = new HashMap<>();
        map1.put("Octavia", 2015);
        map1.put("Superb", 2018);
        printMap(map1);
    }

    public static void printMap(Map<String, Integer> map) {
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.printf("Key: <%s>, Value: <%d>\n", entry.getKey(), entry.getValue());
        }


    }
}
