package cz.java5.p5;

import java.util.ArrayList;
import java.util.List;

public class SDAHashSet<E> implements HashSet<E> {
   private List<E> container = new ArrayList<>();


    @Override
    public HashSet<E> add(E item) {
        container.add(item);
        return this;
    }

    @Override
    public void remove(E item) {
        container.remove(item);
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean contains(E item) {
        return container.contains(item);
    }

    @Override
    public void clear() {
        container.clear();
    }
}
