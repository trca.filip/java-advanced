package cz.java5.p4;

import java.util.*;
import java.util.stream.Stream;

public class Storage {
    Map<String, List<String>> map = new HashMap<>();

    public void addToStorage(String key, String value) {
        List<String> strings = map.get(key);
        if (strings == null) {
            ArrayList<String> values = new ArrayList<>();
            map.put(key, values);
        }
        map.get(key).add(value);
    }

    public void printValues(String key) {
        map.get(key).forEach((value) -> {
            System.out.println(value);
        });
    }

    public void printAll() {
        map.forEach((key, value) -> {
            value.forEach(val -> {
                System.out.println(key + " " + val);
            });
        });
    }

    public void findValues(String value) {
        map.forEach((key, item) -> {
            item.forEach(val -> {
                if (value.equals(val)) {
                    System.out.println(key + " " + val);
                }
            });
        });
    }

    public void findValues2(String value) {
       map.values().parallelStream()
               .flatMap(val->val.stream()).filter(val->val.equals(value)).forEach(System.out::println);
                                                                    //.forEach(s->System.out.println(s));

    }


}
